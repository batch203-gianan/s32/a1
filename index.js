const http = require("http");

const port = 4000;

const server = http.createServer((request, response) =>{
	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {"Content-Type":"application/json"});
		response.end("Welcome to your profile!");
	} 
	else if (request.url == "/course" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "application/json"});
		response.end("Here's our courses available");
	}
	else if(request.url == "/addcourse" && request.method == "POST"){
		response.writeHead(200, {"Content-Type": "application/json"});
		response.end("Add a course to our resources")
	}
	else if(request.url == "/updatecourse" && request.method == "PUT"){
		response.writeHead(200, {"Content-Type": "application/json"});
		response.end("Update a course to our resources");
	}
	else if(request.url == "/archivecourse" && request.method == "DELETE"){
		response.writeHead(200, {"Content-Type": "application/json"});
		response.end("Archive courses to our resources");
	}
	else{
		response.writeHead(200, {"Content-Type":"application/json"});
		response.end("Welcome to Booking System");
	} 
});

server.listen(port);

console.log(`localhost: ${port}`);